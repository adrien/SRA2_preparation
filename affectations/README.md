# Affectation des étudiants à un groupe

## Les fichiers

* `etudiants.csv` contient la liste des étudiants en CSV, avec en première ligne, le header : `prenom,nom,groupe`
* `sujets.tsv` contient la liste des sujets en TSV, avec en première ligne, le header : `titre	site	tags`

    Les tags sont facultatifs, et séparés par des virgules.

## Installation

Seule dépendance Python : `pandas` (inclut `numpy`). Vous pouvez l'installer proprement avec : 

```
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

## Exécution de la lotterie

Considérant des fichiers `etudiants.csv` et `sujets.csv`, exécutez : 

```
$ python lotterie.py [-s <nombre>]
Using following random seed: <nombre>
Successfully wrote affectations_<nombre>.tsv
```