# 9 octobre 2022

	
Le A de SRA fait référence à "Avancés" donc l'idée initiale était de ne pas se contenter de rester en surface mais de creuser en profondeur un sujet technique pour produire un techtalk en demandant aux étudiants i) de comprendre un sujet technique qu'ils ne maitrisent pas nécessairement au départ et ii) de transmettre à leurs camarades ce sujet pour qu'ils le comprennent à leur tour.

Dès lors, j'aimerais éviter tout ce qui se réduit à une "simple" API (RESTful) et centrer cela sur les protocoles applicatifs divers et variés qu'on peut retrouver dans la nature. C'est un travail qui peut nécessiter de creuser un système pour comprendre comment des nœuds répartis au sein d'un réseau arrivent à collaborer.

Au delà des systèmes discutés précédemment, on peut imaginer une présentation des protocoles sous-jacents à WhatsApp/Signal, Spotify ou d'autres systèmes qu'ils cotoyent au quotidien, ou dont ils ont au moins entendu parlé. Comme ils sont en fin de M2, on doit pouvoir leurs demander de jouer les détectives pour comprendre comment ça marche sous le moteur, de documenter leur processus d'investigation, de citer leurs sources et de restituer le résultat de leur enquête à des tiers qui pourront avoir confiance dans ce qui est présenté.

# Bien avant


Comme tu connais (bien) SR1 et SR2, tu n'auras pas de mal à comprendre SRA1 et SRA2. SRA1 est coordonné par Lionel et couvre les aspects application web N-tiers avec du Spring (ça reste client-serveur). L'idée de SRA2 était de se positionner dans la continuité de SR2—tout comme SRA1—et d'aborder les autres formes de répartitions que le CS traditionnel.

En vrac, j'avais noté des mots-clés acronymes comme :

Blockchain ouverte/fermée & BFT
Microservices
EDA

Distributed streaming
Stockage décentralisé
Edge/fog computing

Serverless computing

P2P & DHT
Content PubSub * Spanning Tree    
Gossip
Configuration management - Leader election
Chaos engineering
Counter strike - MMPORG
High frequency trading
Kafka CRDT
ETL

Et/ou des aspects sous un angle plus théoriques avec des séances orientées par thème :

Clock synchronisation / Berkeley also
Message Queues / pubsub / distributed minimum spanning tree
Consensus => Paxos / Raft / Blockchain / Chandra Toueg
DHT => Spotify, Cassandra, Web Torrent
CRDT => Redis
Leader election => Zookeeper / Bully algo / Hirschberg–Sinclair algorithm
Random Peer Sampling?
En sachant que certains aspects sont partiellement abordés en SR1 (clock, consensus, leader...)