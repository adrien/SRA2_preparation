# Préparation du cours de SRA2 - 2022-23

**Pitch :** Votre équipe est chargée par une structure (entreprise, pouvoir public, association...) de réaliser une vidéo de présentation technique sur une application (imposée). Il s'agit d'expliquer le protocole applicatif réparti sur lequel repose le logiciel à l'étude.

## Applications proposées

> Autres : Federated learning (google...), IoT (?), GitHub, Discord, édition collaborative type Framapad/Collabora/OnlyOffice

### Mastodon 

Tags : micro-blogging, federated, ActivityPub

[Site officiel](https://joinmastodon.org/)

### Element 

Tags : chat,E2EE,federated,privacy

[Site officiel](https://element.io)

### Telegram

Tags : chat,E2EE,mobile,privacy

[Site officiel](https://telegram.org/)

### Jitsi

Tags : streaming,video,conference,web,E2EE

[Site officiel](https://jitsi.org/)

Notes : 

* WebRTC vs. SIP : SIP établit des sessions de communications entre des groupes d'utilisateurs, tandis que WebRTC autorise le transfert de media dans les navigateurs. WebRTC a besoin de SIP, car sa fonction s'arrête à l'établissement de session non comprise. Tandis que SIP se passe de WebRTC, même si son usage rend plus simple le développement d'applications de streaming,puisqu'on peut inclure la communication dans un navigateur via WebRTC.

### Tor Browser

Tags : anonymity,E2EE,trustless,decentralised,privacy,web

[Site officiel](https://www.torproject.org/)

### Valorant

Tags: video-game,multiplayer,latency,networking

[Site officiel](https://playvalorant.com/)

Ressources : 

* https://technology.riotgames.com/news/riot-direct-video
* https://technology.riotgames.com/news/peeking-valorants-netcode

### Counter Strike: Global Offensive

Tags : video-game,multiplayer,latency,networking

[Site officiel](https://www.counter-strike.net/)

Ressources : 

* [Vidéo sur le lag Counter Strike de SR1](https://www.youtube.com/watch?v=GX4595KeZyc)
* https://developer.valvesoftware.com/wiki/Source_Multiplayer_Networking

### Sorare

Tags : video-game,multiplayer,collectibles,NFT,blockchain,soccer

[Site officiel](https://sorare.com/)

### Axie Infinity

Tags : video-game,multiplayer,collectibles,NFT,blockchain,fantasy

[Site officiel](https://axieinfinity.com/)

Ressources : 

* [Hack de Axie Infinity sur le Journal du Coin](https://journalducoin.com/nft/hack-axie-inifnity-ronin-sky-mavis/)
* [Hack de Axie Infinity sur LinkedIn](https://www.linkedin.com/pulse/axie-infinity-ronin-decentralization-alexander-c-mueller-phd)

### Dem-Attest-ULille

Tags : blockchain,permissioned,certification

Ressources :

* [Livre blanc](https://www.univ-lille.fr/fileadmin/user_upload/presse/2022/20220114_Livre_blanc_Dem-Attest-ULille_FR.pdf)
* [Bortzmeyer](https://www.bortzmeyer.org/blockchain-inutile.html)

### FileCoin 

Tags : peer-to-peer,IPFS,blockchain,storage,file sharing

[Site officiel](https://filecoin.io/)

### Popcorn Time

Tags : peer-to-peer,decentralised,video,streaming,file sharing

[Site officiel](https://github.com/popcorn-official/popcorn-desktop)

### BitTorrent

Tags : peer-to-peer,decentralised,file sharing

[Site officiel](https://bittorrent.com)

### Netflix 

Tags : video,streaming,content distribution,microservices

[Site officiel](https://www.netflix.com/)

### Twitch 

Tags : video,streaming,live,content distribution,social network

[Site officiel](https://www.twitch.tv/)

### Spotify

Tags : audio,streaming,content distribution,peer-to-peer

[Site officiel](https://www.spotify.com/)

## Exemples de techtalk

* Moxie Marlinspike, [The ecosystem is moving: Challenges for distributed and decentralized technology from the perspective of Signal development](https://media.ccc.de/v/36c3-11086-the_ecosystem_is_moving), CCC, 2019.
* Roger Dingledine, [The Tor Censorship Arms Race The Next Chapter](https://youtu.be/ZB8ODpw_om8), DEF CON 27, 2019.
* Tristan Miller, [Reversing the Original Xbox Live Protocols](https://youtu.be/HLyZfZMu-5E), DEF CON 30, 2022.

Maybe:

* Altas Engineer, [Next, the programmable web browser How the architectural choices and the Lisp language make for an infinitely extensible web browser](https://video.fosdem.org/2020/H.2215/next_web_browser.webm), FOSDEM, 2020.

A regarder : Devoxx

